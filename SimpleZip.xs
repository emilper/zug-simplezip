#define PERL_NO_GET_CONTEXT
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include <stdio.h>
#include <zip.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <libgen.h>

#include "ppport.h"


#define DEBUG 1;


void add_file_to_archive(const char *file_path, zip_t *archive, int add_root_path) {
    FILE *file_pointer;
    zip_int64_t index;
    
    if ((file_pointer = fopen(file_path, "r")) == NULL) {
        croak("can't open input file '%s': %s", file_path, strerror(errno));
    }
    
    zip_source_t *file_source;
    if ( (file_source = zip_source_filep(archive, file_pointer, 0, -1)) == NULL) {
        croak("error creating file source for '%s': %s", file_path, zip_strerror(archive));
    }
                
    if (add_root_path) {
        index = zip_file_add(archive, file_path, file_source, ZIP_FL_OVERWRITE);    
    } else {
        char *file_basename = basename(file_path);
        index = zip_file_add(archive, file_basename, file_source, ZIP_FL_OVERWRITE);    
    }
    
    long made_it = (long) index;
    if ( made_it == -1 ) {
        zip_source_free(file_source);
        croak("failed to add file %s", file_path);
    }
}


int open_destination_file_for_writing(const char *base_path, const char *file_name_string) {
    char path[strlen(base_path) + strlen(file_name_string) + 1];
    
    strcpy(path, base_path);
    strcat(path, "/");
    strcat(path, file_name_string);

    int file_descriptor = open(path, O_RDWR | O_TRUNC | O_CREAT, 0644);
    if (file_descriptor < 0) {
        croak("cound not open %s for writing", path);
    }

    return file_descriptor;
}


void debug_entry_stats( zip_stat_t entry_stats) {
    fprintf(stderr, "File stats for %s\n"
    "\tvalid: %i\n"
    "\tindex: %i\n"
    "\tsize: %i\n"
    "\tcompressed size: %i\n"
    "\tmodification time: %i\n\n",
    entry_stats.name,
    (int)entry_stats.valid,
    (int)entry_stats.index,
    (int)entry_stats.size,
    (int)entry_stats.comp_size,
    (int)entry_stats.mtime);
}


void walk_directory_recursive(const char *path, int level, zip_t *archive, int add_root_path) {
    if (level > 1000) {
        croak("You got 1000 levels deep in the directory tree, there must be some recursive links in there\n");
    }
    
    DIR *dp;
    struct dirent *ep;
    // printf("path is %s\n", path);
    
    dp = opendir (path);
    if (dp != NULL) {
        while (ep = readdir (dp)) {
            
            if (0 == strcmp(ep->d_name, ".") ) {
                //printf("this is the current folder: %s\n", ep->d_name);
                continue;
            }
            
            if (0 == strcmp(ep->d_name, "..") ) {
                // printf("this is the folder above %s\n", ep->d_name);
                continue;
            }
            
            
            if (ep->d_type == DT_DIR ) {
                // printf("name is : %s/%s\t and it is a directory\n", path, ep->d_name);
                char subdir_path[strlen(path) + strlen(ep->d_name) + 1];
                strcpy(subdir_path, path);
                strcat(subdir_path, "/");
                strcat(subdir_path, ep->d_name);
                if (add_root_path) {
                    zip_int64_t index = zip_dir_add(archive, subdir_path, ZIP_FL_ENC_UTF_8);
                } else {
                    zip_int64_t index = zip_dir_add(archive, ep->d_name, ZIP_FL_ENC_UTF_8);
                }
                
                long made_it = (long) index;
                if ( made_it == -1 ) {
                    croak("failed to add directory %s : ", subdir_path, zip_strerror(archive));
                }
                
                walk_directory_recursive(subdir_path, level + 1, archive, add_root_path);

            } else if (ep->d_type == DT_REG ) {
                // printf("name is : %s/%s\t and it is a regular file\n", path, ep->d_name);
                
                char file_path[strlen(path) + strlen(ep->d_name) + 1];
                strcpy(file_path, path);
                strcat(file_path, "/");
                strcat(file_path, ep->d_name);
                // printf("file path is %s\n", file_path);
                
                if (add_root_path) {
                    add_file_to_archive(file_path, archive, 1);
                } else {
                    add_file_to_archive(file_path, archive, 0);
                }

            } else if (ep->d_type == DT_LNK) {
                // printf("name is : %s\t type: %i\n", ep->d_name, ep->d_type);
                fprintf(stderr, "TODO: name is : %s/%s\t and it is a link, not handled yet\n", path, ep->d_name);
            } else {
                fprintf(stderr, "name is : %s/%s\t and I don't know what type this is: %i\n", path, ep->d_name, ep->d_type);
            }
            
        }
        closedir (dp);
    } else {
        fprintf(stderr, "Couldn't open the directory %s", path);
    }
}


int is_directory(const char *file_path) {

    struct stat path_stat;
    stat(file_path, &path_stat);
    return S_ISDIR(path_stat.st_mode);
}

// croak if you can't make the directory and deal with it from perl
void make_destination_dir(const char *dir_path) {
    int dir_made;
    // printf("### before making destination dir %s in make_destination_dir \n", dir_path);
    struct stat st = {0};
    if (stat(dir_path, &st) == -1) {
        if ((dir_made  = mkdir(dir_path, 0700) ) == -1 ) {
            croak("failed to make the dir %s; error is %s", dir_path, strerror(errno));
        }
    } else {
        croak("path %s exists, giving up", dir_path);
    }
}

void make_extracted_dir(const char *base_path, const char *extracted_path) {
    char path[strlen(base_path) + strlen(extracted_path) + 1];
    
    strcpy(path, base_path);
    strcat(path, "/");
    strcat(path, extracted_path);

    int dir_made = mkdir(path, 0700);
    if (dir_made == -1 ) {
        char *error_string = strerror(errno);
        croak("error is %s", error_string);
    }
}


MODULE = Zug::SimpleZip		PACKAGE = Zug::SimpleZip		

AV*
list_entries(path)
    const char* path;
CODE:
    AV* entries_list = newAV();
    // printf("zip path is %s\n", path);
    
    int errors = 0;
    
    zip_t* archive = zip_open(path, ZIP_RDONLY, &errors);
    // printf("errors are %i\n", errors);

    if (errors) {
        char buf[100];
        zip_error_to_str(buf, sizeof(buf), errors, errno);
        fprintf(stderr, "something wrong while opening zip archive `%s': %s\n", path, buf);
    }

    if (archive == NULL) {
        fprintf(stderr, "archive is null\n");
    } else {
        // printf("number of entries %i\n", (int) zip_get_num_entries(archive,ZIP_FL_UNCHANGED));
        int index;
        int number_of_entries = zip_get_num_entries(archive, ZIP_FL_UNCHANGED);
     
        for (index = 0; index < number_of_entries; index++) {
            const char *file_name_string = zip_get_name(archive, index, ZIP_FL_ENC_GUESS);
            
            SV* file_name = newSVpvn(file_name_string, strlen(file_name_string));
            printf(" file name = %s\n", file_name_string);
            av_push(entries_list, file_name);
        }
    }
    
    if (zip_close(archive) != 0) {
        croak("failed to close the archive");
    }
    
    RETVAL = entries_list;
OUTPUT:
    RETVAL


int
extract_all(destination_path, zip_file_path)
    const char* destination_path;
    const char* zip_file_path;
CODE:

    int errors = 0; // if not initialized deliberately gets initialized anyway but not with what I expect
    zip_t* archive = zip_open(zip_file_path, ZIP_RDONLY, &errors);

    if (errors) {
        char buf[100];
        zip_error_to_str(buf, sizeof(buf), errors, errno);
        croak("something wrong while opening zip archive `%s': %s", zip_file_path, buf);
    }

    if (archive == NULL) {
        croak("archive is null");
    } else {
        // printf("\n\n### before making destination dir %s\n", destination_path);
        make_destination_dir(destination_path);
        // printf("\n\n### after making destination dir %s\n", destination_path);
        
        int index;
        int number_of_entries = zip_get_num_entries(archive, ZIP_FL_UNCHANGED);
     
        for (index = 0; index < number_of_entries; index++) {
            
            zip_stat_t stats_buff;
            if (zip_stat_index(archive, index, 0, &stats_buff) != 0 ) {
                croak("cannot read the stats for index %i in archive %s", index, zip_file_path);
            }
            
            const char *file_name_string = zip_get_name(archive, index, ZIP_FL_ENC_GUESS);
            // printf("\n### file to extract %s\n\n", file_name_string);
            int name_length = strlen(file_name_string);            
            if (file_name_string[name_length - 1] == '/') {
                make_extracted_dir(destination_path, file_name_string);
            } else {
                zip_file_t *file_entry = zip_fopen_index(archive, index, 0);
                if (!file_entry) {
                    croak(" file %s cound not be opened", file_name_string);
                }
                
                int destination_file = open_destination_file_for_writing(destination_path, file_name_string);
                int sum = 0;
                char entry_buffer[1024];
                int read_length;
                ssize_t wrote_length;
                while (sum != stats_buff.size) {
                    read_length = zip_fread(file_entry, entry_buffer, 1024);
                    if (read_length < 0) {
                        croak("failed to read from archived file %s", file_name_string);
                    }
                    wrote_length = write(destination_file, entry_buffer, read_length);
                    if (wrote_length != read_length) {
                        croak("something wrong when writing file to disk: read length %i is not equal with write length%i\n", read_length, wrote_length);
                    }
                    sum += read_length;
                }
                close(destination_file);
                zip_fclose(file_entry);
            }
        }
    }
    
    if (zip_close(archive) != 0) {
        croak("failed to close the archive");
    }
    
    // printf("### after closing the archive\n");
    RETVAL = 1;
OUTPUT:
    RETVAL


int
archive_folder(path, archive_path, add_root_path)
    const char* path;
    const char* archive_path;
    int add_root_path;
CODE:

    int level = 0;
    int error = 0;
    zip_t *archive = zip_open(archive_path, ZIP_CREATE, &error);
    if (error) {
        croak("failed to create the archive with path %s", archive_path);
    }

    if (add_root_path) {
        zip_int64_t first_index = zip_dir_add(archive, path, ZIP_FL_ENC_UTF_8);
        if ( first_index == -1 ) {
            croak("failed to add directory %s : %s\n", path, zip_strerror(archive));
        }
    }
    
    walk_directory_recursive(path, level, archive, add_root_path);
    
    if (zip_close(archive) == -1) {
        croak("can't close zip archive");
    }

    RETVAL = 1;
OUTPUT:
    RETVAL

int
archive_file(path, archive_path)
    const char* path;
    const char* archive_path;
CODE:
    // printf("file is %s archive name %s\n", path, archive_path);
    int level = 0;
    int error = 0;
    zip_t *archive = zip_open(archive_path, ZIP_CREATE, &error);

    if (error) {
        croak("failed to create the archive with path %s", archive_path);
    }

    add_file_to_archive(path, archive, 0);
    
    if (zip_close(archive) == -1) {
        croak("can't close zip archive");
    }

    RETVAL = 1;
OUTPUT:
    RETVAL

