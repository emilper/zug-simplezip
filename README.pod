=head1 SYNOPSIS

    use Zug::SimpleZip;
    use Data::Dumper;
    
    my $folder = "tests";
    my $path = "/tmp/blah.zip";
    my $extra_file = "Makefile.PL";
    my $path_single_file = "/tmp/blah_single.zip";
    my $extract_to = "/tmp/blah_extracted";

    Zug::SimpleZip::archive_folder($folder, $path);
    
    if (Zug::SimpleZip::file_exists($path)) {
         my $result = Zug::SimpleZip::list_entries($path);
        print STDERR Dumper($result);
    } else {
       print STDERR "file " . $path . " not found\n";    
    }

    Zug::SimpleZip::archive_file($extra_file,$path_single_file);
    
    if (Zug::SimpleZip::file_exists($path_single_file)) {
        my $result = Zug::SimpleZip::list_entries($path_single_file);
    } else {
       print STDERR "file " . $path_single_file . " not found\n";    
    }    
    
    Zug::SimpleZip::extract_all($extract_to, $path);

    1;

=head1 STATUS

sort of works, can extract archives, compress folders, compress single files

only procedural interface, I don't want to lose the microseconds I am not sure I gained by using libzip ;)


=head1 PERFORMANCE NOTES

there appears to be no performance gain over Archive::Zip on large files, and it is a bit slower on smaller files 

=head1 DESCRIPTION

create and extract zip archives with libzip

=head1 SUBROUTINES

=head2 archive_folder

Arguments:

=over 4

=item folder to archive

path to folder, absolute or relative

=item archive path 

for example test.zip

=back

Return: 1 on success, croaks on error

=head2 archive_file

Arguments:

=over 4

=item file to archive 

path to file, absolute or relative

=item archive path 

for example test.zip

=back

Return: 1 on success, croaks on error


=head2 extract_all

Arguments:

=over 4

=item folder to extract to

path to folder, absolute or relative

=item archive path

absolute or relative

=back

Returns: 1 on success, croaks on error

=head2 list_entries

Arguments:

=over 4

=item path to archive

absolute or relative

=back

Returns: arrayref of filenames and folders included in the archive on success, croaks on error

=head1 TODO

=over 4

=item add a file to an existing archive

=item extract a particular file from an archive, by name or by index

=item replace a file in an archive

=item delete a file from an archive

=item compression levels

=back

=head1 SEE ALSO

Please use Archive::Zip if you need to manipulate zip files. Zug::SimpleZip is barely started and will do only very basic operations.

=head1 AUTHOR

Emil Perhinschi, emilper at gmail.com

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2015 by Emil Perhinschi

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.20.2 or,
at your option, any later version of Perl 5 you may have available.


