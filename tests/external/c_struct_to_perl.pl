#!/usr/bin/env perl -w

# http://www.perlmonks.org/bare/?node_id=135567

use Inline C;
use strict; $|++;

my $recv = test_recv_struct();
print "recv: " . join ( ",", unpack ( "iiiiiZ*", $recv ) ) . "\n";

my $send = pack ( "iZ*", 1, "string two" );
test_send_struct ( $send );

__END__
__C__

struct recv {
  int flag;
  int b[4];
  char nextpass[10];
};
typedef struct recv t_recv;

struct send {
  int answer;
  char prevpass[10];
};
typedef struct send t_send;

//-----

SV* test_recv_struct() {
  t_recv* foo = malloc ( sizeof(t_recv) );
  foo->flag = 1;
  foo->b[0] = 2;
  foo->b[1] = 3;
  foo->b[2] = 4;
  foo->b[3] = 5;
  sprintf( foo->nextpass, "a string" );
  return newSVpv ( (char*)foo, sizeof(t_recv) );
}

void test_send_struct ( char* perl_packed ) {
  t_send* foo = perl_packed;
  printf ( "send: %d,%s\n", foo->answer, foo->prevpass );
}

