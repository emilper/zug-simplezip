#!/usr/bin/env perl

use strict;
use warnings;


# use ExtUtils::testlib;
use Zug::SimpleZip;
use Data::Dumper;

my $folder = "tests";
my $path = "/tmp/blah.zip";
my $path_with_root_folder = "/tmp/blah_with_root_folder.zip";
my $extra_file = "./README.pod";
my $path_single_file = "/tmp/blah_single.zip";
my $extract_to = "/tmp/blah_extracted";

eval {
    Zug::SimpleZip::archive_folder($folder, $path, 0);
    print STDERR "\n=================================\n created archive " . $path . " without adding root folder\n\n\n";

    Zug::SimpleZip::archive_folder($folder, $path_with_root_folder, 1);
    print STDERR "\n=================================\ncreated archive " . $path . " with root folder\n\n\n";
    
    if (Zug::SimpleZip::file_exists($path)) {
        print STDERR "file found\n";
        my $result = Zug::SimpleZip::list_entries($path);
        print STDERR Dumper($result);
    } else {
       print STDERR "file " . $path . " not found\n";    
    }
    print STDERR "\n=================================\nlisted archive $path \n\n\n";
    
    
    Zug::SimpleZip::archive_file($extra_file,$path_single_file);
    print STDERR "\n=================================\ncreated archive " . $path_single_file . "\n\n\n";
    
    if (Zug::SimpleZip::file_exists($path_single_file)) {
        print STDERR "file found\n";
        my $result = Zug::SimpleZip::list_entries($path_single_file);
        print STDERR Dumper($result);
    } else {
       print STDERR "file " . $path_single_file . " not found\n";    
    }    
    
    Zug::SimpleZip::extract_all($extract_to, $path);
    print STDERR "\n=================================\nextracted archive $path \n\n\n";
    1;
} or do {
    print STDERR Dumper $@ if $@;
    print STDERR "croaked\n";
};
