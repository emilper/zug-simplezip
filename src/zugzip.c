#include <stdio.h>
#include <zip.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "zugzip.h"

int list_entries( const char* path) {

    printf("zip path is %s\n", path);
    struct stat file_stats;
    if (stat(path, &file_stats) == -1) {
        perror("stat");
        return 1;
    }
    
    print_file_stats(file_stats);
    int errors;
    
    zip_t* archive = zip_open(path, ZIP_RDONLY, &errors);
    printf("errors are %i\n", errors);
    if (archive == NULL) {
        printf("archive is null\n");
        return 1;
    }
    
    printf("number of entries %i\n", (int) zip_get_num_entries(archive,ZIP_FL_UNCHANGED));
    zip_uint64_t index;
    for (index = 0; index < zip_get_num_entries(archive, ZIP_FL_UNCHANGED); index++) {
        const char *file_name = zip_get_name(archive, index, ZIP_FL_ENC_GUESS);
        printf("file name is %s\n", file_name);
    }
    return 0;
}


void print_file_stats( struct stat file_stats) {

   printf("File type:                ");

    switch (file_stats.st_mode & S_IFMT) {
        case S_IFBLK:  printf("block device\n");            break;
        case S_IFCHR:  printf("character device\n");        break;
        case S_IFDIR:  printf("directory\n");               break;
        case S_IFIFO:  printf("FIFO/pipe\n");               break;
        case S_IFLNK:  printf("symlink\n");                 break;
        case S_IFREG:  printf("regular file\n");            break;
        case S_IFSOCK: printf("socket\n");                  break;
        default:       printf("unknown?\n");                break;
    }

    printf("I-node number:            %ld\n", (long) file_stats.st_ino);

    printf("Mode:                     %lo (octal)\n",
            (unsigned long) file_stats.st_mode);

    printf("Link count:               %ld\n", (long) file_stats.st_nlink);
    printf("Ownership:                UID=%ld   GID=%ld\n",
            (long) file_stats.st_uid, (long) file_stats.st_gid);

    printf("Preferred I/O block size: %ld bytes\n",
            (long) file_stats.st_blksize);
    printf("File size:                %lld bytes\n",
            (long long) file_stats.st_size);
    printf("Blocks allocated:         %lld\n",
            (long long) file_stats.st_blocks);

    printf("Last status change:       %s", ctime(&file_stats.st_ctime));
    printf("Last file access:         %s", ctime(&file_stats.st_atime));
    printf("Last file modification:   %s", ctime(&file_stats.st_mtime));
}